import asyncio
import pprint

import dbus_next
from dbus_next.aio import MessageBus

async def get_media_players(bus):
    meta_introspection = await bus.introspect("org.freedesktop.DBus", "/org/freedesktop/DBus")
    meta_proxy = bus.get_proxy_object('org.freedesktop.DBus', "/org/freedesktop/DBus", meta_introspection)
    meta_interface = meta_proxy.get_interface("org.freedesktop.DBus")
    
    names = []
    for name in await meta_interface.call_list_names():
        if name.startswith("org.mpris.MediaPlayer2"):
            names.append(name)
    return names

async def get_mediaplayer_interface(bus, name):
    introspection = await bus.introspect(name, "/org/mpris/MediaPlayer2")
    #print([x.name for x in introspection.interfaces])
    proxy = bus.get_proxy_object(name, "/org/mpris/MediaPlayer2", introspection)
    interface = proxy.get_interface("org.mpris.MediaPlayer2.Player")
    return interface

async def get_metadata(bus, name):
    try:
        interface = await get_mediaplayer_interface(bus, name)
    except dbus_next.errors.InterfaceNotFoundError:
        return None
    return await interface.get_metadata()
    

async def main():
    bus = await MessageBus().connect()
    names = await get_media_players(bus)

    for name in names:
        metadata = await get_metadata(bus, name)
        if metadata is None:
            # media players which don't reveal this info don't have this interface
            # so just ignore it
            continue
        print(", ".join(metadata["xesam:artist"].value), "-", metadata["xesam:title"].value)

if __name__ == "__main__":
    asyncio.run(main())
