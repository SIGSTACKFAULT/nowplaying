import asyncio
import functools
import json

import click
import dbus_next
from dbus_next.aio import MessageBus

import nowplaying.dbus_nonsense as dbus_nonsense

class VariantEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, dbus_next.signature.Variant):
            return obj.value
        return json.JSONEncoder.default(self, obj)

def coro_wrapper(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        asyncio.get_event_loop().run_until_complete(f(*args, **kwargs))
    return wrapper

@click.group()
@click.pass_context
@coro_wrapper
async def main(ctx):
    ctx.ensure_object(dict)
    ctx.obj["BUS"] = await MessageBus().connect()
    ctx.obj["METADATAS"] = await get_metadatas(ctx.obj["BUS"])


@main.command()
@click.pass_context
@coro_wrapper
async def list(ctx):
    """list all dbus names which seem to support getting their current track"""
    for name in ctx.obj["METADATAS"]:
        print(name)


# these get used twice, so assign them all to variables
json_option = click.option("-j", "--json", "human_readable", flag_value=False, default=True,
              help="json mode: print one json object per line, with all the data that gets sent back.")
name_option = click.option("--show-bus-name", flag_value=True,
                           help="without --json: show bus name for each result")

@main.command(name="get")
@json_option
@name_option
@click.option("--wildcard", flag_value=True,
              help="use shell globbing for bus name. incompatible with --fuzzy.")
@click.option("--fuzzy", flag_value=True,
              help="fuzzy search. incompatible with --wildcard")
@click.argument("bus_name", metavar="BUSNAME")
@click.pass_context
@coro_wrapper
async def get(ctx, bus_name:str, human_readable=False, show_bus_name=False, wildcard=False, fuzzy=False):
    """print the current track for the given bus name, with various formatting option. optionally supports wildcards."""
    if fuzzy:
        for name in ctx.obj["METADATAS"]:
            if bus_name in name:
                bus_name = name
                break
    metadata = ctx.obj["METADATAS"][bus_name]
    if human_readable:
        print_metadata(bus_name, metadata, show_name=show_bus_name)
    else:
        print(json.dumps(metadata, cls=VariantEncoder))

@main.command(name="get-all")
@json_option
@name_option
@click.pass_context
@coro_wrapper
async def get_all(ctx, human_readable, show_bus_name):
    """print all current tracks, with various formatting options"""
    if human_readable:
        for name, metadata in ctx.obj["METADATAS"].items():
            print_metadata(name, metadata, show_name=show_bus_name)
    else:
        print(json.dumps(metadatas, cls=VariantEncoder))


async def get_metadatas(bus):
    return {
        name: await dbus_nonsense.get_metadata(bus, name)
        for name in await dbus_nonsense.get_media_players(bus)
    }

def print_metadata(name, metadata, show_name=False):
    if metadata is None:
        return
    if show_name: print(f"{name}: ", end="")
    print(format_metadata(metadata))


def format_metadata(metadata):
    """print info for the given name on the given bus in human-readable format."""
    artists = ", ".join(metadata["xesam:artist"].value)
    title = metadata["xesam:title"].value
    return f"{artists} - {title}"
    

if __name__ == "__main__":
    main()

